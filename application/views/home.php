    <div class="redboa-side-content">
        <header id="home" class="header pos-re slider-fade">
            <div class="owl-carousel owl-theme">
                <div class="item bg-img" data-overlay-dark="7" data-background="assets/img/blog/sate-1.jpg">
                    <div class="container text-center v-middle caption">
                        <div class="icon"><span class="icon flaticon-022-tray"></span></div>
                        <h4>Welcome Sate Steak</h4>
                        <h1>The Pub</h1> <a href="#menu" class="butn butn-bg"><span>Our Menu</span></a>
                    </div>
                </div>
                <div class="item bg-img" data-overlay-dark="7" data-background="assets/img/blog/sate-4.jpg">
                    <div class="container text-center v-middle caption">
                        <div class="icon"><span class="icon flaticon-001-wine"></span></div>
                        <h4>Welcome to Sate Steak</h4>
                        <h1>The Pub</h1> <a href="#about" class="butn butn-bg"><span>About Us</span></a>
                    </div>
                </div>
                <div class="item bg-img" data-overlay-dark="7" data-background="assets/img/blog/sate-2.jpg">
                    <div class="container text-center v-middle caption">
                        <div class="icon"><span class="icon flaticon-029-cow-1"></span></div>
                        <h4>Welcome to Sate Steak</h4>
                        <h1>The Pub</h1> <a href="#gallery" class="butn butn-bg"><span>Gallery</span></a>
                    </div>
                </div>
            </div>
        </header>
        <section id="about" class="about pt-120 pb-120">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 offset-md-1">
                        <h6 class="sub-title">Sate Steak</h6>
                        <h4>The <span>PUB</span></h4>
                        <p>Sate Steak The PUB berdiri sejak tahun 1973 di sebuah Bar yang lokasinya termasuk Pionir di Kuta dengan nama The PUB Restaurant berlokasi di jalan Buni sari Kuta Bali.  
                            Sate The PUB terbuat dari Daging Sapi pilihan dipadukan dengan Ramuan rahasia istimewa yang sudah diwariskan secara turun temurun sampai sekarang. Sate The PUB mempunyai ciri khas daging “big size” 10 stick dengan berat 300 gram. 
                            Sate The PUB mempunyai bumbu Sweet soya sauce serta rempah bumbu ramuan sendiri yang tidak seperti sate pada umumnya yang hanya menggunakan  sauce kacang.Pada tahun 2017 Restaurant The PUB berubah konsep dan nama berubah menjadi HOGWARTZ The PUB, restaurant yang bernuasana atau bertema Harry Potter dan Restaurant Penyihir. 
                            HOGWARTZ The PUB  menjadi restaurant yang unik di bali karena beberapa makanan special dihidangkan dengan sentuhan Magic sehingga akhirnya mendapatkan penghargaan sebagai restaurant terunik no 3 di Bali dan No 6 di Asia sebagai Restaurant bertema Harry potter satu satunya di Bali. Jika anda datang ke restaurant Hogwartz The PUB untuk membeli Sate The PUB, anda akan di hidangkan Sate Steak dengan Hot-plate dan akan di sajikan dengan nuansa sihir yang unik.
                            Sekarang Sensasi Sate Steak bisa dibeli secara online melalui market place (Gojek,Tokopedia,Bukalapak,Shopee dll), bahkan anda juga bisa membeli Sate Steak  matang (siap saji) khusus untuk area Bali. Hidangan sudah include Kentang dan Salad.  Selain itu, anda juga bisa membeli Sate Steak dalam keadaan Frozen yang bisa anda  masak di rumah atau dimana saja dan kapan saja karena daging sate  sudah di vacumm packing dan di beku agar awet dan tahan lama.

  </p><br />
                    </div>
                    <div class="col-md-4">
                        <div class="about-img mb-20">
                            <div class="img"><img src="assets/img/slider/slider2.jpeg" alt="img"></div>
                        </div>
                        <div class="about-img">
                            <div class="img"><img src="assets/img/slider/slider3.jpeg" alt="img"></div>
                        </div>
                        <br>
                        <div class="about-img">
                            <div class="img"><img src="assets/img/slider/slider1.jpeg" alt="img"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="menu" class="menus menu section-padding bg-img bg-fixed pos-re pb-120" data-overlay-dark="7" data-background="assets/img/banner.jpg">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 offset-md-1 mb-40 text-center">
                        <h6 class="sub-title">The PUB</h6>
                        <h4 class="title">OUR MENU</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="row">
                            <div class="tabs-icon mb-40 col-md-10 offset-md-1 text-center">
                                <div class="owl-carousel owl-theme">
                                    <div id="tab-1" class="active item"> <span class="icon flaticon-022-tray"></span>
                                        <h6>Main</h6>
                                    </div>
                                    <div id="tab-3" class="item"> <span class="icon flaticon-013-salad"></span>
                                        <h6>Frozen</h6>
                                    </div>
                                </div>
                            </div>
                            <div class="menus-content col-md-12">
                                <div id="tab-1-content" class="cont active">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="menu-info">
                                                <h5>Beef Sate 5 Stick  <span class="price">75K</span></h5>
                                                <p>150 Gr Meat Beef include Rise and served with salad</p>
                                            </div>
                                            <div class="menu-info">
                                                <h5>Chicken 5 Stick <span class="price">45K$</span></h5>
                                                <p>Fried onion rings, smoked aioli</p>
                                            </div>
                                        </div>
                                        <div class="col-md-5 offset-md-2">
                                            <div class="menu-info">
                                                <h5>Mix Sate 5 Stick <span class="price">50K$</span></h5>
                                                <p>75 Gr Chic and 50 Gr Beef  include Rise and served with salad</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="tab-3-content" class="cont">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="menu-info">
                                                <h5>Beef  Sate 5 Stick <span class="price">65K</span></h5>
                                            </div>
                                        </div>
                                        <div class="col-md-5 offset-md-2">
                                            <div class="menu-info">
                                                <h5>Chicken Sate 5 Stick <span class="price">35K</span></h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="gallery" class="portfolio pt-120 pos-re">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 offset-md-1 mb-40 text-center">
                        <h6 class="sub-title">REDBOA</h6>
                        <h4 class="title">Gallery</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 offset-md-1 text-center">
                <div class="row">
                    <div class="filtering text-center mb-20 col-sm-12">
                        <span data-filter='*' class="active">All</span>
                        <span data-filter='.mains'>Mains</span>
                        <span data-filter='.salads'>Salads</span>
                        <span data-filter='.breakfast'>Breakfast</span>
                    </div>
                    <div class="clearfix"></div>
                    <div class="gallery text-center full-width">
                        <div class="col-md-4 items mains">
                           <a href="assets/img/gallery/1.jpg" class="popimg">
                            <div class="item-img">
                                <img src="assets/img/gallery/1.jpg" alt="image">
                                <div class="item-img-overlay valign">
                                    <div class="overlay-info full-width vertical-center">
                                        <h6>Mains</h6>
                                    </div>
                                </div>
                            </div>
                            </a>
                        </div>
                        <div class="col-md-4 items salads">
                           <a href="assets/img/gallery/3.jpg" class="popimg">
                            <div class="item-img">
                                <img src="assets/img/gallery/3.jpg" alt="image">
                                <div class="item-img-overlay valign">
                                    <div class="overlay-info full-width vertical-center">
                                        <h6>Salads</h6>
                                    </div>
                                </div>
                            </div>
                            </a>
                        </div>
                        <div class="col-md-4 items breakfast">
                           <a href="assets/img/gallery/5.jpg" class="popimg">
                            <div class="item-img">
                                <img src="assets/img/gallery/5.jpg" alt="image">
                                <div class="item-img-overlay valign">
                                    <div class="overlay-info full-width vertical-center">
                                        <h6>Breakfast</h6>
                                    </div>
                                </div>
                            </div>
                            </a>
                        </div>
                        <div class="col-md-4 items mains">
                           <a href="assets/img/gallery/2.jpg" class="popimg">
                            <div class="item-img">
                                <img src="assets/img/gallery/2.jpg" alt="image">
                                <div class="item-img-overlay valign">
                                    <div class="overlay-info full-width vertical-center">
                                        <h6>Mains</h6>
                                    </div>
                                </div>
                            </div>
                            </a>
                        </div>
                        <div class="col-md-4 items salads">
                           <a href="assets/img/gallery/4.jpg" class="popimg">
                            <div class="item-img">
                                <img src="assets/img/gallery/4.jpg" alt="image">
                                <div class="item-img-overlay valign">
                                    <div class="overlay-info full-width vertical-center">
                                        <h6>Salads</h6>
                                    </div>
                                </div>
                            </div>
                            </a>
                        </div>
                        <div class="col-md-4 items breakfast">
                           <a href="assets/img/gallery/6.jpg" class="popimg">
                            <div class="item-img">
                                <img src="assets/img/gallery/6.jpg" alt="image">
                                <div class="item-img-overlay valign">
                                    <div class="overlay-info full-width vertical-center">
                                        <h6>Breakfast</h6>
                                    </div>
                                </div>
                            </div>
                            </a>
                        </div>
                        <div class="clear-fix"></div>
                    </div>
                </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="testimonials section-padding bg-img bg-fixed pos-re pb-120" data-overlay-dark="7" data-background="assets/img/banner.jpg">
            <div class="container">
                <div class="row">
                    <div class="offset-md-3 col-md-6">
                        <div class="owl-carousel owl-theme text-center">
                            <div class="item">
                                <div class="client-area">
                                    <h6>Jonathan Lee</h6> <span>California, USA</span>
                                </div>
                                <p>The name REDBOA, in principle, quite fully describes the restaurant's concept: red meat and red wine - what else is needed for a steakhouse!</p>
                            </div>
                            <div class="item">
                                <div class="client-area">
                                    <h6>Emma White</h6> <span>Madrid, Spain</span>
                                </div>
                                <p>The name REDBOA, in principle, quite fully describes the restaurant's concept: red meat and red wine - what else is needed for a steakhouse!</p>
                            </div>
                            <div class="item">
                                <div class="client-area">
                                    <h6>Drana Moss</h6> <span>Roma, Italy</span>
                                </div>
                                <p>The name REDBOA, in principle, quite fully describes the restaurant's concept: red meat and red wine - what else is needed for a steakhouse!</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="blog" class="blog pt-120 pb-120">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 offset-md-1 mb-60 text-center">
                        <h6 class="sub-title">Our Latest Blog</h6>
                        <h4 class="title">Blog</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="item">
                                    <div class="post-img">
                                        <div class="img"> <img src="assets/img/blog/1.jpg" alt="img"> </div>
                                    </div>
                                    <div class="cont">
                                        <h6><a href="blog.html">Meat and Poultry Recipes</a></h6>
                                        <p>Maecenas viverra lorem eget ex mollis the ute gravida est luctus. Maecenas eu tellus vel nunc ullamcorper iaculis non varius turpis.</p> <a href="blog.html" class="more">Details <i class="ti-angle-double-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="item">
                                    <div class="post-img">
                                        <div class="img"> <img src="assets/img/blog/2.jpg" alt="img"> </div>
                                    </div>
                                    <div class="cont">
                                        <h6><a href="post.html">Easy Mongolian Beef Recipes</a></h6>
                                        <p>Maecenas viverra lorem eget ex mollis the ute gravida est luctus. Maecenas eu tellus vel nunc ullamcorper iaculis non varius turpis.</p> <a href="post.html" class="more">Details <i class="ti-angle-double-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="item">
                                    <div class="post-img">
                                        <div class="img"> <img src="assets/img/blog/3.jpg" alt="img"> </div>
                                    </div>
                                    <div class="cont">
                                        <h6><a href="post.html">Recipe for a Delicious Cake</a></h6>
                                        <p>Maecenas viverra lorem eget ex mollis the ute gravida est luctus. Maecenas eu tellus vel nunc ullamcorper iaculis non varius turpis.</p> <a href="post.html" class="more">Details <i class="ti-angle-double-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center"> <a href="blog.html" class="butn butn-bg"><span>BLOG ARCHIEVE</span></a> </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="banner section-padding bg-img bg-fixed pos-re text-center" data-overlay-dark="8" data-background="assets/img/banner.jpg">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h6 class="sub-title">Rezervation</h6>
                        <h4>Take-away Service</h4>
                        <p>MONDAY - SUNDAY : 5 PM - 10:30 PM</p> <a href="index.html#" class="butn butn-bg"><span><i class="ti-phone"></i> +1 843-577-4444</span></a>
                    </div>
                </div>
            </div>
        </div>
        <section id="contact" class="contact pt-120 pb-120">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 offset-md-1 mb-60 text-center">
                        <h6 class="sub-title">Get In Touch</h6>
                        <h4 class="title">Contact Us</h4>
                    </div>
                    <div class="col-md-10 offset-md-1">
                        <div class="contact-info">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="item">
                                        <div class="cont">
                                            <h5>OUR ADDRESS</h5>
                                            <p>24 King Street, Charleston
                                                <br />South Carolina 29401</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="item">
                                        <div class="cont">
                                            <h5>PHONE & EMAIL</h5>
                                            <h6>+1 843-577-4444</h6>
                                            <p>reservations@redboa.com</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="item">
                                        <div class="cont">
                                            <h5>OPENING HOURS</h5>
                                            <p>MONDAY - SUNDAY
                                                <br />5 PM - 10:30 PM</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
