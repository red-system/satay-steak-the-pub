<!DOCTYPE html>
<html lang="id">

<head>
	<title>Satay Steak The Pub Kuta Bali</title>
	<meta name="keywords" content="sate enak kuta, sate enak bali, steak enak bali, steak enak kuta"/>
	<meta name="description" content="sate steak terbaik di kuta bali">
	<meta charset="UTF-8">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
	<meta content="telephone=no" name="format-detection">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<meta name="revisit-after" content="2 days"/>
	<meta name="robots" content="index, follow"/>
	<meta name="rating" content="General"/>
	<meta name="author" content="sataysteakthepub"/>
	<meta name="MSSmartTagsPreventParsing" content="true"/>
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="reply-to" content="book@satesteakthepub.com">
	<meta http-equiv="expires" content="Mon, 28 Jul <?php echo date('Y')+1;?> 11:12:01 GMT">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link href="<?php echo base_url('assets/img/favicon.png');?>" rel="icon" sizes="192x192">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/satesteakthepub.min.css'); ?>"/>
</head>

<body>
<div class="redboa-fixed-sidebar redboa-sidebar-left">
	<div class="redboa-header-container">
		<div class="logo image-custom">
			<a href="<?php echo site_url(); ?>">
				<img src="<?php echo base_url(); ?>assets/img/logo.png" class="img-fluid" alt="Satay Steak The Pub"/>
			</a>
			<div class="img-fluid">
				<?php echo anchor($this->lang->switch_uri('id'), '<img style="width:20px;height:15px;"src="'.base_url().'assets/img/indonesian-flag.png" alt="Indonesian Language">') ?>
				&nbsp;
				&nbsp;
				<?php echo anchor($this->lang->switch_uri('en'), '<img style="width:20px;height:15px;"src="'.base_url().'assets/img/english-flag.png" alt="England Language">') ?>
			</div>

		</div>
		<div class="redboa-burger-menu">
			<div class="redboa-line-menu redboa-line-half redboa-first-line"></div>
			<div class="redboa-line-menu"></div>
			<div class="redboa-line-menu redboa-line-half redboa-last-line"></div>
		</div>
		<nav class="redboa-menu-fixed">
			<ul>
				<li><a href="<?php echo base_url() . $lang; ?>#home">Beranda</a></li>
				<li><a href="<?php echo base_url() . $lang;; ?>#about">Tentang Kami</a></li>
				<li><a href="<?php echo base_url() . $lang;; ?>#menu">Menu</a></li>
				<li><a href="<?php echo base_url() . $lang;; ?>#gallery">Galeri</a></li>
				<li><a href="<?php echo base_url() . $lang;; ?>#contact">Kontak</a></li>
			</ul>

			<div class="mobile-show">
				<a href="<?php echo base_url(); ?>">
					<img src="<?php echo base_url(); ?>assets/img/logo.png"
														  class="img-fluid" alt=""/>
				</a>
				<div class="img-fluid text-center">
					<?php echo anchor($this->lang->switch_uri('id'), '<img style="width:20px;height:15px;"src="' . base_url() . 'assets/img/indonesian-flag.png"></img>') ?>
					<?php echo anchor($this->lang->switch_uri('en'), '<img style="width:20px;height:15px;"src="' . base_url() . 'assets/img/english-flag.png"></img>') ?>
				</div>
			</div>

		</nav>

		<div class="redboa-footer-sidebar">
			<p><span>Chat Sekarang</span></p>
			<a target="blank" href="https://wa.me/+6281238482225"><h6>Whatsapp</h6></a>
		</div>
	</div>
</div>
