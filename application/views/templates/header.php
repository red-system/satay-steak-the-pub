<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>Satay Steak The Pub</title>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets/img/favicon.png');?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css');?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.menu.css');?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/magnific-popup.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/animate.min.css');?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/flaticon.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/themify-icons.css');?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/owl.carousel.min.css');?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/owl.theme.default.min.css');?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css');?>" />
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-144098545-1"></script>
</head>

<body>
    <!-- Preloader -->
    <div id="preloader">
        <div class="loading-area">
            <div class="logo"><img src="<?php echo base_url();?>assets/img/logo.png" alt=""></div>
            <span>loading...</span>
        </div>
       
        <div class="left-side"></div>
        <div class="right-side"></div>
    </div>
    <!-- Sidebar -->
    <div class="redboa-fixed-sidebar redboa-sidebar-left">
        <div class="redboa-header-container">
            <!--Logo-->
            <div class="logo image-custom">
                <a href="<?php echo base_url();?>"> <img src="<?php echo base_url();?>assets/img/logo.png" class="img-fluid" alt="" />
                    <!-- <h1> <span>STEAK & WINE</span></h1> -->
                </a>
                <div class="img-fluid"><?php echo anchor($this->lang->switch_uri('id'), '<img style="width:20px;height:15px;"src="'.base_url().'assets/img/indonesian-flag.png"></img>') ?> <?php echo anchor($this->lang->switch_uri('en'), '<img style="width:20px;height:15px;"src="'.base_url().'assets/img/english-flag.png"></img>') ?> </div>
                
            </div>
            <!-- Burger menu -->
            <div class="redboa-burger-menu">
                <div class="redboa-line-menu redboa-line-half redboa-first-line"></div>
                <div class="redboa-line-menu"></div>
                <div class="redboa-line-menu redboa-line-half redboa-last-line"></div>
            </div>
            <!--Navigation menu-->
            <nav class="redboa-menu-fixed">
                <ul>
                    
                    <li><a href="<?php echo base_url().$lang;?>#home">Home</a></li>
                    <li><a href="<?php echo base_url().$lang;;?>#about">About Us</a></li>
                    <li><a href="<?php echo base_url().$lang;;?>#menu">Menu</a></li>
                    <li><a href="<?php echo base_url().$lang;;?>#gallery">Gallery</a></li>
                    <li><a href="<?php echo base_url().$lang;;?>#blog">Blog</a></li>
                    <li><a href="<?php echo base_url().$lang;;?>#contact">Contact</a></li>
                </ul>
                
            </nav>
            
            <!-- Rezervation -->
            <div class="redboa-footer-sidebar">
                <p><span>Rezervation</span></p>
                <h6>+1 843-577-4444</h6>
            </div>
        </div>
    </div>
