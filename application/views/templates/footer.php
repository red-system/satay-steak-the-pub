<footer class="text-center pos-re">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="social"> <a href="index.html#"><i class="ti-facebook"></i></a> <a href="https://instagram.com/satelegend.thepub?igshid=gux8nv12y455"><i class="ti-instagram"></i></a> </div>
                        <p><small>© 2020 REDSYSTEM. All right reserved.</small></p>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <script src="<?php echo base_url('assets/js/satesteakthepub.min.js'); ?>"></script>
</body>
</html>
