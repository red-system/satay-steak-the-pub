<div class="redboa-side-content">
	<header id="home" class="header pos-re slider-fade">
		<div class="owl-carousel owl-theme">
			<div class="item bg-img" data-overlay-dark="7"
				 data-background="<?php echo base_url() ?>assets/img/blog/sate-1.jpg">

				<div class="container text-center v-middle caption">
					<div class="icon"><span class="icon flaticon-022-tray"></span></div>
					<h4>Selamat Datang di Steak</h4>
					<h1>The Pub</h1> <a href="#getnow" class="butn butn-bg"><span>Dapatkan Sekarang</span></a>
				</div>
			</div>
			<div class="item bg-img" data-overlay-dark="7"
				 data-background="<?php echo base_url() ?>assets/img/blog/sate-4.jpg">
				<div class="container text-center v-middle caption">
					<div class="icon"><span class="icon flaticon-001-wine"></span></div>
					<h4>Selamat Datang di Sate Steak</h4>
					<h1>The Pub</h1> <a href="#getnow" class="butn butn-bg"><span>Dapatkan Sekarang</span></a>
				</div>
			</div>
			<div class="item bg-img" data-overlay-dark="7"
				 data-background="<?php echo base_url() ?>assets/img/blog/sate-2.jpg">
				<div class="container text-center v-middle caption">
					<div class="icon"><span class="icon flaticon-029-cow-1"></span></div>
					<h4>Selamat Datang di Sate Steak</h4>
					<h1>The Pub</h1> <a href="#getnow" class="butn butn-bg"><span>Dapatkan Sekarang</span></a>
				</div>
			</div>
		</div>
	</header>
	<section id="about" class="about pt-120 pb-120">
		<div class="container">
			<div class="row">
				<div class="col-md-6 offset-md-1">
					<h6 class="sub-title">Sate Steak</h6>
					<h4>The <span>PUB</span></h4>
					<p>Sate Steak The PUB berdiri sejak tahun 1973 di sebuah Bar yang lokasinya termasuk Pionir di Kuta
						dengan nama The PUB Restaurant berlokasi di jalan Buni sari Kuta Bali.
						Sate The PUB terbuat dari Daging Sapi pilihan dipadukan dengan Ramuan rahasia istimewa yang
						sudah diwariskan secara turun temurun sampai sekarang. Sate The PUB mempunyai ciri khas daging
						“big size” 10 stick dengan berat 300 gram.
						Sate The PUB mempunyai bumbu Sweet soya sauce serta rempah bumbu ramuan sendiri yang tidak
						seperti sate pada umumnya yang hanya menggunakan sauce kacang.Pada tahun 2017 Restaurant The PUB
						berubah konsep dan nama berubah menjadi HOGWARTZ The PUB, restaurant yang bernuasana atau
						bertema Harry Potter dan Restaurant Penyihir.
						HOGWARTZ The PUB menjadi restaurant yang unik di bali karena beberapa makanan special
						dihidangkan dengan sentuhan Magic sehingga akhirnya mendapatkan penghargaan sebagai restaurant
						terunik no 3 di Bali dan No 6 di Asia sebagai Restaurant bertema Harry potter satu satunya di
						Bali. Jika anda datang ke restaurant Hogwartz The PUB untuk membeli Sate The PUB, anda akan di
						hidangkan Sate Steak dengan Hot-plate dan akan di sajikan dengan nuansa sihir yang unik.
						Sekarang Sensasi Sate Steak bisa dibeli secara online melalui market place
						(Gojek,Tokopedia,Bukalapak,Shopee dll), bahkan anda juga bisa membeli Sate Steak matang (siap
						saji) khusus untuk area Bali. Hidangan sudah include Kentang dan Salad. Selain itu, anda juga
						bisa membeli Sate Steak dalam keadaan Frozen yang bisa anda masak di rumah atau dimana saja dan
						kapan saja karena daging sate sudah di vacumm packing dan di beku agar awet dan tahan lama.

					</p><br/>
				</div>
				<div class="col-md-4">
					<div class="about-img mb-20">
						<div class="img"><img src="<?php echo base_url() ?>assets/img/slider/slider2.jpeg" alt="img">
						</div>
					</div>
					<div class="about-img">
						<div class="img"><img src="<?php echo base_url() ?>assets/img/slider/slider3.jpeg" alt="img">
						</div>
					</div>
					<br>
					<div class="about-img">
						<div class="img"><img src="<?php echo base_url() ?>assets/img/slider/slider1.jpeg" alt="img">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="menu" class="menus menu section-padding bg-img bg-fixed pos-re pb-120" data-overlay-dark="7"
			 data-background="assets/img/banner.jpg">
		<div class="container">
			<div class="row">
				<div class="col-md-10 offset-md-1 mb-40 text-center">
					<h6 class="sub-title">The PUB</h6>
					<h4 class="title">Menu Kami</h4>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 offset-md-1">
					<div class="row">
						<div class="tabs-icon mb-40 col-md-10 offset-md-1 text-center">
							<div class="owl-carousel owl-theme">
								<div id="tab-1" class="active item"><span class="icon flaticon-022-tray"></span>
									<h6>Main</h6>
								</div>
								<div id="tab-3" class="item"><span class="icon flaticon-013-salad"></span>
									<h6>Frozen</h6>
								</div>
							</div>
						</div>
						<div class="menus-content col-md-12">
							<div id="tab-1-content" class="cont active">
								<div class="row">
									<div class="col-md-5">
										<div class="menu-info">
											<div class="post-img">
												<div class="img">
													<img
														src="<?php echo base_url(); ?>assets/img/menus/satay-chicken-meat.jpg"
														alt="img">
												</div>
											</div>
											<h5>Chicken Sate, 5 Stick 150 gr<span class="price">50K</span></h5>
											<p>Grilled High Quality Chicken Meat on Skewers With Sweet Soya Sauce,
												served with Potato Wedges / Mashed Potato and Salad</p>

										</div>
										<br>
										<div class="menu-info">
											<div class="post-img">
												<div class="img">
													<img
														src="<?php echo base_url(); ?>assets/img/menus/satay-mix.jpg"
														alt="img">
												</div>
											</div>
											<h5>Mix Sate, 10 Stick 300 gr (150gr chicken, 150gr beef)<span
													class="price">110</span></h5>
											<p>Grilled High Quality Beef and Chicken Meat on Skewers With Sweet Soya
												Sauce, served with Potato Wedges / Mashed Potato and Salad</p>
										</div>
									</div>
									<div class="col-md-5 offset-md-2">
										<div class="menu-info">
											<div class="post-img">
												<div class="img">
													<img
														src="<?php echo base_url(); ?>assets/img/menus/satay-beef-meat.jpg"
														alt="img">
												</div>
											</div>
											<h5>Beef Sate, 5 Stick 150 gr<span class="price">75K</span></h5>
											<p>Grilled High Quality Beef Meat on Skewers With Sweet Soya Sauce, served
												with Potato Wedges / Mashed Potato and Salad</p>
										</div>
										<br>
									</div>
								</div>
							</div>
							<div id="tab-3-content" class="cont">
								<div class="row">
									<div class="col-md-5">
										<div class="menu-info">
											<div class="post-img">
												<img
													src="<?php echo base_url(); ?>assets/img/menus/satay-frozen.jpg"
													alt="img">
											</div>
											<h5>Beef Sate (5 Stick, 150 gr) <span class="price">65K</span></h5>
										</div>
										<br/>
										<div class="menu-info">
											<div class="post-img">
												<img
													src="<?php echo base_url(); ?>assets/img/menus/satay-frozen.jpg"
													alt="img">
											</div>
											<h5>Chicken Sate (5 Stick, 150 gr) <span class="price">45K</span></h5>
										</div>
									</div>
									<div class="col-md-5 offset-md-2">
										<div class="menu-info">
											<div class="post-img">
												<img
													src="<?php echo base_url(); ?>assets/img/menus/satay-frozen.jpg"
													alt="img">
											</div>
											<h5>Mix Sate (10 Stick, 300 gr) <span class="price">100K</span></h5>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="gallery" class="portfolio pt-120 pos-re">
		<div class="container">
			<div class="row">
				<div class="col-md-10 offset-md-1 mb-40 text-center">
					<h6 class="sub-title">REDBOA</h6>
					<h4 class="title">Galeri</h4>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 offset-md-1 text-center">
					<div class="row">
						<div class="filtering text-center mb-20 col-sm-12">
							<span data-filter='*' class="active">All</span>
						</div>
						<div class="clearfix"></div>
						<div class="gallery text-center full-width">
							<div class="col-md-4 items mains">
								<a href="<?php echo base_url(); ?>assets/img/menus/1.jpeg" class="popimg">
									<div class="item-img">
										<img src="<?php echo base_url(); ?>assets/img/menus/thumbs/1.jpeg" alt="image">
										<div class="item-img-overlay valign">
											<div class="overlay-info full-width vertical-center">
												<h6>Mains</h6>
											</div>
										</div>
									</div>
								</a>
							</div>
							<div class="col-md-4 items mains">
								<a href="<?php echo base_url(); ?>assets/img/menus/2.jpeg" class="popimg">
									<div class="item-img">
										<img src="<?php echo base_url(); ?>assets/img/menus/thumbs/2.jpeg" alt="image">
										<div class="item-img-overlay valign">
											<div class="overlay-info full-width vertical-center">
												<h6>Mains</h6>
											</div>
										</div>
									</div>
								</a>
							</div>
							<div class="col-md-4 items mains">
								<a href="<?php echo base_url(); ?>assets/img/menus/3.jpeg" class="popimg">
									<div class="item-img">
										<img src="<?php echo base_url(); ?>assets/img/menus/thumbs/3.jpeg" alt="image">
										<div class="item-img-overlay valign">
											<div class="overlay-info full-width vertical-center">
												<h6>Mains</h6>
											</div>
										</div>
									</div>
								</a>
							</div>
							<div class="col-md-4 items mains">
								<a href="<?php echo base_url(); ?>assets/img/menus/4.jpeg" class="popimg">
									<div class="item-img">
										<img src="<?php echo base_url(); ?>assets/img/menus/thumbs/4.jpeg" alt="image">
										<div class="item-img-overlay valign">
											<div class="overlay-info full-width vertical-center">
												<h6>Mains</h6>
											</div>
										</div>
									</div>
								</a>
							</div>
							<div class="col-md-4 items mains">
								<a href="<?php echo base_url(); ?>assets/img/menus/5.jpeg" class="popimg">
									<div class="item-img">
										<img src="<?php echo base_url(); ?>assets/img/menus/thumbs/5.jpeg" alt="image">
										<div class="item-img-overlay valign">
											<div class="overlay-info full-width vertical-center">
												<h6>Mains</h6>
											</div>
										</div>
									</div>
								</a>
							</div>
							<div class="col-md-4 items mains">
								<a href="<?php echo base_url(); ?>assets/img/menus/6.jpeg" class="popimg">
									<div class="item-img">
										<img src="<?php echo base_url(); ?>assets/img/menus/thumbs/6.jpeg" alt="image">
										<div class="item-img-overlay valign">
											<div class="overlay-info full-width vertical-center">
												<h6>Mains</h6>
											</div>
										</div>
									</div>
								</a>
							</div>
							<div class="clear-fix"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="testimonials section-padding bg-img bg-fixed pos-re pb-120" data-overlay-dark="7"
			 data-background="assets/img/banner.jpg">
		<div class="container">
			<div class="row">
				<div class="offset-md-3 col-md-6">
					<div class="owl-carousel owl-theme text-center">
						<div class="item">
							<div class="client-area">
								<h6>Reza Aditya</h6> <span>Jakarta</span>
							</div>
							<p>Kalo di Bali gw pasti makan ini nih SateLegend ThePub, ini sate sapinya terbaik nih!, nih
								liat tuh gede gede broo! dan kabar baiknya sate SateLegend ThePub ini ada Frozen Food
								nya tuhh, bisa dikirim kemanapun yang diluar Bali yuk order</p>
							<img src="<?php echo base_url('assets/img/testi-1.jpeg') ?>" width="200"
								 style="width: 200px !important; margin: auto">
						</div>
						<div class="item">
							<div class="client-area">
								<h6>Icha Anisa</h6> <span>Jakarta</span>
							</div>
							<p>Makan siang hari ini ditemani Satelegend The pub, ini yang udah jadinya, ini sapi ya ada
								kentangnya juga sayurannya lengkap dan ini yang frozennya. Mau di bawa ke Jakarta atau
								kemana pun tenang ini aman kok tetep enak sampai di kalian. Empuk banget sumpah</p>
							<img src="<?php echo base_url('assets/img/testi-2.jpeg') ?>" width="200"
								 style="width: 200px !important; margin: auto">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<div class="banner section-padding bg-img bg-fixed pos-re text-center" data-overlay-dark="8"
		 data-background="assets/img/banner.jpg">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h6 class="sub-title">Pesan Sekarang</h6>
					<h4>Di Toko Online Favorit Anda</h4>
					<a href="http://google.com"><img style="width:100px"
													 src="<?php echo base_url(); ?>assets/img/logo-tokopedia.png"></img>
						<span>  &nbsp &nbsp</span></a>
					<a href=""><img style="width:45px" src="<?php echo base_url(); ?>assets/img/logo-shopee.png"></img></a>
				</div>
			</div>
		</div>
	</div>
	<section id="contact" class="contact pt-120 pb-120">
		<div class="container">
			<div class="row">
				<div class="col-md-10 offset-md-1 mb-60 text-center">
					<h6 class="sub-title">Get In Touch</h6>
					<h4 class="title">Contact Us</h4>
				</div>
				<div class="col-md-10 offset-md-1">
					<div class="contact-info">
						<div class="row">
							<div class="col-md-4">
								<div class="item">
									<div class="cont">
										<h5>OUR ADDRESS</h5>
										<p>Jalan Buni sari
											<br/>Kuta Bali</p>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="item">
									<div class="cont">
										<h5>PHONE & EMAIL</h5>
										<h6>+6281238482225</h6>
										<a href="mailto:book@satesteakthepub.com"><p>book@satesteakthepub.com</p></a>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="item">
									<div class="cont">
										<h5>OPENING HOURS</h5>
										<p>Everyday
											<br/>6 AM - 10:30 PM</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
