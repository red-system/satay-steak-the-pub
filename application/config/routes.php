<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['id/blog'] = 'home/blog';
$route['translate_uri_dashes'] = FALSE;
$route['^id/(.+)$'] = "$1";
$route['^id$'] = $route['default_controller'];
$route['^en/(.+)$'] = "$1";
$route['^en$'] = $route['default_controller'];