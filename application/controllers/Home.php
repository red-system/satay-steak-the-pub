<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function index()
	{
		//echo $this->lang->lang();
		$data['lang'] = $this->lang->lang();
		
		if($this->lang->lang() == 'en'){
			$this->load->view('templates/header-en',$data);
			$this->load->view('home-en');
		} else {
			$this->load->view('templates/header-id',$data);
			$this->load->view('home-id');
		}
		
		$this->load->view('templates/footer');
	}
    
    public function blog()
	{
		//echo $this->lang->lang();
		$this->load->view('templates/header');
		$this->load->view('blog');
		$this->load->view('templates/footer');
	}
}
