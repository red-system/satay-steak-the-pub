module.exports = function (grunt) {

    var jsFiles = [
        'assets/js/jquery-3.0.0.min.js',
        'assets/js/jquery-migrate-3.0.0.min.js',
        'assets/js/popper.min.js',
        'assets/js/bootstrap.min.js',
        'assets/js/scrollIt.min.js',
        'assets/js/jquery.waypoints.min.js',
        'assets/js/owl.carousel.min.js',
        'assets/js/jquery.stellar.min.js',
        'assets/js/validator.js',
        'assets/js/autotype.js',
        'assets/js/isotope.pkgd.min.js',
        'assets/js/jquery.magnific-popup.min.js',
        'assets/js/bootstrap.menu.js',

        'assets/js/scripts.js',
    ];
    var cssFiles = [
        'assets/css/bootstrap.min.css',
        'assets/css/bootstrap.menu.css',
        'assets/css/magnific-popup.css',
        'assets/css/animate.min.css',
        'assets/css/flaticon.css',
        'assets/css/themify-icons.css',
        'assets/css/owl.carousel.min.css',
        'assets/css/owl.theme.default.min.css',
        'assets/css/style.css'
    ];
    var allFiles = jsFiles.concat(cssFiles); // merge js & css files directory

    grunt.initConfig({
        jsDistDir: 'assets/js/',
        cssDistDir: 'assets/css/',
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            js: {
                options: {
                    separator: ';'
                },
                src: jsFiles,
                dest: '<%=jsDistDir%><%= pkg.name %>.js'
            },
            css: {
                src: cssFiles,
                dest: '<%=cssDistDir%><%= pkg.name %>.css'
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%=grunt.template.today("dd-mm-yyyy") %> */\n'
            },
            dist: {
                files: {
                    '<%=jsDistDir%><%= pkg.name %>.min.js': ['<%= concat.js.dest %>']
                }
            }
        },
        cssmin: {
            add_banner: {
                options: {
                    // banner: '/*! <%= pkg.name %> <%=grunt.template.today("dd-mm-yyyy") %> */\n'
                },
                files: {
                    '<%=cssDistDir%><%= pkg.name %>.min.css': ['<%= concat.css.dest %>']
                }
            }
        },
        watch: {
            files: allFiles,
            tasks: ['concat', 'uglify', 'cssmin']
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', [
        'concat',
        'uglify',
        'cssmin',
        'watch'
    ]);

};
